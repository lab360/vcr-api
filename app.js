require('dotenv').config();
const port = process.env.PORT || 3000;
const express = require('express');
const app = express();

/** Sentry **/
if(process.env.SENTRY_STATUS === 'on'){
    console.log('Sentry running...');
    const sentry = require('@sentry/node') ;
    sentry.init({ dsn: process.env.SENTRY_DSN});
    app.use(sentry.Handlers.requestHandler());
    app.use(sentry.Handlers.errorHandler());
}


/** Express Settings **/
app.use(express.json({ limit: '5mb' }));
app.use(express.urlencoded({ limit: '5mb', extended: true }));
app.use(express.static('public'));

/** API Routes Definition **/
app.use('/', require('./routes/web'));
app.use('/api', require('./routes/api'));

// swagger definition
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerSpec = swaggerJSDoc({
    swaggerDefinition: {
        info: {
            title: 'LabConnect API',
            description: '',
        },
        host: 'localhost:3000',
        basePath: '/api',
        securityDefinitions: {
            BearerToken: {
                type: 'apiKey',
                in: 'header',
                name: 'bearer'
            }
        },
    },
    apis: [
        './models/*.js',
        './controllers/api/*.js',
        './routes/api.js'
    ],
});
app.get('/docs-raw', (req, res) => res.json(swaggerSpec));

const swaggerUi = require('swagger-ui-express');
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec, {
    customCss: '.swagger-ui .topbar { background-color: rgb(44,68,155); height: 55px;} .topbar-wrapper img[alt="Swagger UI"], .topbar-wrapper span {visibility: collapse;} .topbar-wrapper .link:after { content: url("../lab.png"); position: absolute;top: 5px;}'
}));



app.listen(port, () => {
    console.log(`LabConnect started on port ${port}`);

    const mixpanel = require('./services/mixpanel');
    mixpanel.track('app-started');
});