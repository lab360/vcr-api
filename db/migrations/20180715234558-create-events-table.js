'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('events', {
      id: Sequelize.UUIDV4
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('events');
  }
};
