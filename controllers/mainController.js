const jwt = require('jsonwebtoken');
const models = require('../models');
const helpers = require('../helpers');
const passport = require('../services/jwt');

module.exports = {
    index: (req, res) => {
        res.send('silence is golden');
    },
    auth: (req, res) => {
        if(req.body.master !== process.env.MASTER_KEY){
            return res.status(401).send({});
        }

        models.user.create(req.body).then(user => {
            const plainUser = user.get({ plain: true });
            res.json({
                ...plainUser,
                access_token: jwt.sign(plainUser.id, process.env.JWT_KEY)
            });
        });
    },
    token: (req, res) => {
        models.user.findById(req.params.id).then(user => {
            const plainUser = user.get({ plain: true });

            req.login(plainUser, {session: false}, (err) => {
                if (err) {
                    res.send(err);
                }

                const token = jwt.sign(plainUser.id, process.env.JWT_KEY);
                return res.json({user: plainUser, token});
            });
        });
    },
    docs: (req, res) => {
    }
};

