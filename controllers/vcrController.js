const models = require('../models');
const vcr = require('../services/vcr');
const jira = require('../services/jira');
const mixpanel = require('../services/mixpanel');

module.exports = {
    status: (req, res) => {
        Promise.all([
            models.log.all({ where: {key: 'log-tags'}, limit: 10, order: [['created_at', 'DESC']], raw: true }),
            models.log.all({ where: {key: 'ticket-created'}, limit: 10, order: [['created_at', 'DESC']], raw: true }),
            models.log.all({ where: {key: 'comment-created'}, limit: 10, order: [['created_at', 'DESC']], raw: true })
        ]).then(([log_tags, ticket_created, comment_created]) => {

            res.render('vcr', {
                api: [
                    {
                        title: 'Inserir Bipagem (etiquetas)',
                        status: log_tags.length !== 0,
                        endpoint: 'http://www.rodoviasullog.com.br/recleituratot.php',
                        key: vcr.constants.LOG_TAGS,
                        last_sent: log_tags[0]
                    }
                ],
                webhooks: [
                    {
                        title: 'Boletim de Ocorrência (criação)',
                        status: ticket_created.length !== 0,
                        key: vcr.constants.TICKET_CREATED,
                        last_received: ticket_created[0]
                    },
                    {
                        title: 'Tratativas (criação)',
                        status: comment_created.length !== 0,
                        key: vcr.constants.COMMENT_CREATED,
                        last_received: comment_created[0]
                    }
                ]
            })
        });
    },
    tickets: (req, res) => {
        vcr.tickets.find({
            branch: req.body.filaber,
            number: req.body.bo
        }).then(vcr_ticket => {

            jira.issue.create({
                fields: {
                    project: {
                        id: 10038
                    },
                    summary: `${vcr_ticket.number} - ${vcr_ticket.origin}`,
                    issuetype: {
                        id: vcr_ticket.type.jira_id
                    },
                    labels: ["vcr", vcr_ticket.branch_assigned],
                    description: {
                        type: "doc",
                        version: 1,
                        content: [
                            {
                                type: "paragraph",
                                content: [
                                    {
                                        type: "text",
                                        text: vcr_ticket.title
                                    }
                                ]
                            }
                        ]
                    },
                }
            }).then(issue => {
                mixpanel.track('ticket-created', {
                    ...issue
                });

                vcr_ticket.update({ jira_id: issue.id}).then(() => {
                    return res.json(issue);
                });
            });
        });
    },
    comments: (req, res) => {
        res.json({ok: true});
    }
};