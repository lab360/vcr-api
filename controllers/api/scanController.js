const models = require('./../../models');
const moment = require('moment');
const _ = require('lodash');
const vcr = require('./../../services/vcr');
const slack = require('./../../services/slack');
const mixpanel = require('./../../services/mixpanel');

async function onFinishedScan(scan){
    const manifest = await models.manifest.findById(scan.manifest_id);

    Promise.all([
        vcr.api.fetch_tags(manifest.vcr_number, manifest.vcr_branch, manifest.vcr_series),
        models.user.findById(scan.user_id),
    ]).then(([vcr_tags, user]) => {

        let difference = _.difference(
            vcr_tags.map(tag => tag.etiq),
            scan.tags,
        );

        let total_time = moment(scan.updated_at)
                .diff(scan.created_at, 'minutes');

        if(difference.length === 0){
            slack.send({
                text: `*${user.vcr_username}* finalizou uma bipagem agora para o manifesto *${manifest.vcr_branch+'/'+manifest.vcr_number}* sem não conformidades!`,
                channel: '#pendencia',
                username: 'Rodovia Sul',
                attachments: [{
                    fallback: 'Required plain-text summary of the attachment.',
                    color: '#36a64f',
                    title_link: 'https://api.slack.com/',
                    fields: [
                        {
                            'title': 'Tempo & Total',
                            'value': `${total_time} minutos, ${scan.tags.length} etiquetas`,
                            'short': true,
                        },
                    ],
                    footer: 'LabConnect',
                    footer_icon: 'http://rodoviasul.lab360.co/logo.png',
                }],
            });

            return;
        }

        const grouped = _.chain(difference)
            .map(item => {
                const [type, document, etiq, volume] = item.split('x');
                return {
                    document: type+'x'+document,
                    etiq,
                    volume
                };
            })
            .groupBy('document')
            .map((tags, document) => {

                const [t, d] =  document.split('x');

                return {
                    type: t,
                    document: d,
                    tags: _.map(tags, tag => tag.volume),
                };
            })
            .groupBy('type')
            .value();


        const NFEpromises = _.map(grouped.N, (item, type) => {
            return vcr.nfe(manifest.vcr_number, item.document);
        });

        Promise.all(NFEpromises).then(([...results]) => {
            const documents = _.groupBy(results, 'number');

            slack.send({
                text: `*${user.vcr_username}* finalizou uma bipagem agora para o manifesto *${manifest.vcr_branch+'/'+manifest.vcr_number}*, porém houveram não conformidades:`,
                channel: '#pendencia',
                username: 'Rodovia Sul',
                attachments: grouped.N.map(item => {

                    const document = documents[item.document][0];

                    return {
                        fallback: 'Required plain-text summary of the attachment.',
                        color: 'danger',
                        title: `${document.client.name}`,
                        title_link: 'https://api.slack.com/',
                        text: `*Volumes Faltantes:* ${'`'+_.join(item.tags, '`, `')+'`'}`,
                        fields: [
                            {
                                'title': 'NF-e',
                                'value': document.number,
                                'short': true,
                            },
                            {
                                'title': 'CT-e',
                                'value': document.cte,
                                'short': true,
                            },
                        ],
                        footer: 'LabConnect',
                        footer_icon: 'http://rodoviasul.lab360.co/logo.png',
                    };
                }),
            });
        });
    });
}


module.exports = {
    index: (req, res) => {
        models.scan.all({
            where: {
                status: 'active',
            },
        }).then(lab360_scans => {
            res.json(lab360_scans);
        });
    },
    create: (req, res) => {
        models.scan.create({
            user_id: req.body.user_id,
            manifest_id: req.body.manifest_id,
            status: req.body.status,
            type: req.body.type,
            branch: req.body.branch,
            tags: []
        }).then(lab360_scan => {
            res.json(lab360_scan);
        });
    },
    show: (req, res) => {
        models.scan.findById(req.params.scanID).then(lab360_scan => {
            res.json(lab360_scan);
        });
    },
    update: async (req, res) => {

        const [status, results] = await models.scan.update(req.body, {where: {id: req.params.scanID}, returning: true})
            .catch(err => {
                res.status(400).json({
                    error: 400,
                    message: err.name,
                });
            });

        if (!status) {
            res.status(404).json({
                error: 404,
                message: 'scan not found',
            });
        }

        const scan = results[0];
        res.status(200).send(scan);

        if (scan.status === 'finished') {
            onFinishedScan(scan);
        }
    },

    logEverything: async (req, res) => {
        const scans = await models.scan.findAll({
            include: [
                {model: models.manifest, as: 'manifest'},
                {model: models.user, as: 'user'},
            ]
        });
        let tags = 0;

        await scans.map(scan => {
            const hydratedTags = scan.tags.map(tag => {

                tags = tags+1;

                return {
                    filial: scan.manifest.vcr_branch,
                    filiallocal: scan.branch,
                    serie: scan.manifest.vcr_series,
                    manifesto: scan.manifest.vcr_number,
                    tipoman: 'M', // M ou R
                    tipoope: scan.type === 'load' ? 'C' : 'D', // C ou D
                    barra: tag.qr,
                    usuario: scan.user.vcr_username
                };


            });

            vcr.api.log_scan(hydratedTags).then(() => {
                mixpanel.track('tags-saved-vcr', {count: hydratedTags.length});
            }).catch(err => console.log(err.message));
        });

        res.send({'total_tags': tags});
    },

    log: async (req, res) => {

        if(req.params.scanID === undefined || req.params.scanID === null){
            res.status(400).json({
                status: 'error',
                message: 'ID parameter required'
            });
            return;
        }

        if (req.body.scanned.length === 0) {
            res.status(200).json({
                status: 'ok',
                tags: 0,
            });
            return;
        }

        // const scan = models.scan.findById(req.params.scanID);
        // const tags = _.valuesIn(_.merge(
        //     _.keyBy(scan.tags, 'qr'),
        //     _.keyBy(req.body.scanned, 'qr')
        // ));
        //
        // await models.scan.update({tags}, {
        //     where: {id: req.params.scanID}
        // }).catch(err => {
        //     res.status(400).json({
        //         error: 400,
        //         message: err.message,
        //     });
        // });

        res.status(200).json({
            status: 'ok',
        });

        models.scan.findById(req.params.scanID, {
            include: [
                {model: models.manifest, as: 'manifest'},
                {model: models.user, as: 'user'},
            ],
        }).then(scan => {
            const hydratedTags = scan.tags.map(tag => {
                return {
                    filial: scan.manifest.vcr_branch,
                    filiallocal: scan.branch,
                    serie: scan.manifest.vcr_series,
                    manifesto: scan.manifest.vcr_number,
                    tipoman: 'M', // M ou R
                    tipoope: scan.type === 'load' ? 'C' : 'D', // C ou D
                    barra: tag.qr,
                    usuario: scan.user.vcr_username
                };
            });

            vcr.api.log_scan(hydratedTags).then(() => {
                mixpanel.track('tags-saved-vcr', {count: hydratedTags.length});
            }).catch(err => console.log(err.message));
        });
    }
};
