const models = require('../../models');

module.exports = {
    index: (req, res) => {
        models.dock.all().then(dock => {
            res.send(dock);
        });
    },
    show: (req, res) => {
        models.dock.findById(req.params.dockID).then(dock => {
            res.send(dock);
        });
    },
    update: (req, res) => {
        models.dock.update(req.body, {where: {id: req.params.dockID}}).then(dock => {
            res.send(dock);
        });
    }
};