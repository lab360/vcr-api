/**
 * @swagger
 * definitions:
 *   Vehicle:
 *     properties:
 *       id:
 *         type: string
 *         format: uuid
 *       plate:
 *         type: string
 *       brand:
 *         type: string
 *       year:
 *         type: integer
 *       model:
 *         type: string
 *       city:
 *         type: string
 *       state:
 *         type: string
 *       axis:
 *         type: string
 *       capacity:
 *         type: string
 *       renavam:
 *         type: string
 *       antt:
 *         type: string
 *       created_at:
 *          type: string
 *          format: date
 *       updated_at:
 *          type: string
 *          format: date
 */

const {merge} = require('../../helpers');
const models = require('../../models');
const vcr = require('../../services/vcr');

module.exports = {
    search: (req, res) => {
        vcr.vehicles.find({
            plate: req.params.plate,
        }).then(vcr_vehicle => {
            models.vehicle.findOrCreate({
                where: {
                    plate: vcr_vehicle.plate
                }
            }).then(l360_vehicle => {
                res.send(merge(l360_vehicle[0], vcr_vehicle))
            });
        });
    },
    show: (req, res) => {
        models.vehicle.findById(req.params.vehicleID).then(l360_vehicle => {
            vcr.vehicles.find(l360_vehicle.plate).then(vcr_vehicle => {
                res.send(merge(l360_vehicle, vcr_vehicle));
            })
        });
    }
};