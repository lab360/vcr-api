const vcr = require('../../services/vcr');
const models = require('../../models');

const {merge} = require('../../helpers');

module.exports = {
    create: (req, res) => {
        models.user.create({
            team: req.params.team,
            name: req.params.name,
            email: req.params.email
        }).then(created => res.send(created));
    },
    show: (req, res) => {
        // Start by finding it in our database
        models.user.findById(req.params.userID).then(lab360_user => {
            vcr.users.find({ username: lab360_user.vcr_username }).then(vcr_user => {
                res.send(merge(lab360_user, vcr_user));
            });
        }).catch(error => {
            res.status(404).send();
        });
    }
};
