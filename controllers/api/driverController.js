/**
 * @swagger
 * definitions:
 *   Driver:
 *     properties:
 *       id:
 *         type: string
 *         format: uuid
 *       name:
 *         type: string
 *       phone:
 *         type: string
 *       license_category:
 *         type: string
 *       license_number:
 *         type: string
 *       cpf:
 *         type: string
 *       type:
 *         type: string
 *       created_at:
 *          type: string
 *          format: date
 *       updated_at:
 *          type: string
 *          format: date
 */

const {merge} = require('../../helpers');
const models = require('../../models');
const vcr = require('../../services/vcr');

module.exports = {
    search: (req, res) => {
        vcr.drivers.find({
            cpf: req.params.cpf,
        }).then(vcr_driver => {
            models.driver.findOrCreate({
                where: {
                    vcr_cpf: vcr_driver.cpf
                }
            }).then(lab360_driver => {
                res.send(merge(lab360_driver[0], vcr_driver))
            });
        });
    },
    show: (req, res) => {
        models.driver.findById(req.params.driverID).then(lab360_driver => {
            vcr.drivers.find(lab360_driver.cpf).then(vcr_driver => {
                res.send(merge(lab360_driver, vcr_driver));
            })
        });
    }
};