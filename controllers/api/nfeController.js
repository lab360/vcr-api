const vcr = require('../../services/vcr');
module.exports = {
    show: (req, res) => {
        vcr.nfes.find({ id: req.params.nfeID }).then(nfe => {
            res.json(nfe);
        })
    }
};