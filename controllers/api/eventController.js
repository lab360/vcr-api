const models = require('../../models');
module.exports = {
    index: (req, res) => {
      models.event.all({where: req.params.where}, events => {
          res.send(events);
      });
    },
    create: (req, res) => {
        models.event.create({
            type: 'TEST',
            dock: 1
        }, created => {
            res.send({
                status: 'ok',
                ...created
            });
        });
    },
    show: (req, res) => {
        models.event.findOne({ id: req.params.eventID }, event => {
            res.send(event);
        });
    }
};