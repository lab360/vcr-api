const mixpanel = require('../../services/mixpanel');
const slack = require('../../services/slack');
const twilio = require('../../services/twilio');
const vcr = require('../../services/vcr');

const _ = require('lodash');

const {shipment} = require('../../models');

const TWILIO_FROM = '+555140426757';
const AGENT_CONFIRM_FLOW = 'FW676330f3a0dafd9e8adf7c120a39d1fc';
const CLIENT_CONFIRM_FLOW = 'FW3606c2e4f77a6a2a7472c73d7e899548';

module.exports = {
    client: {
        confirm: (req, res) => {
            mixpanel.track('client-confirmed', {
                shipment_id: req.params.shipmentID
            });

            shipment.findById(req.params.shipmentID).then(lab360_shipment => {
                lab360_shipment.update({status: 'client-confirmed'});
            });

            // Starts the Agent Confirm flow and registers an event to Mixpanel.
            twilio.studio.flows(AGENT_CONFIRM_FLOW).executions.create({
                to: '+5555981143698'
            }).then(execution => {

                mixpanel.track('agent-flow-started', {
                    shipment_id: req.params.shipmentID,
                    flow_sid: execution.flow_sid
                });
            });

            res.send({});

        },
        decline: (req, res) => {
            mixpanel.track('client-declined', {
                shipment_id: req.params.shipmentID
            });

            shipment.findById(req.params.shipmentID).then(lab360_shipment => {
                lab360_shipment.update({status: 'client-declined'});
            });

            res.send({});
        }
    },
    agent: {
        confirm: (req, res) => {
            mixpanel.track('agent-confirmed', {
                shipment_id: req.params.shipmentID
            });

            shipment.findById(req.params.shipmentID).then(lab360_shipment => {
                lab360_shipment.update({status: 'agent-confirmed'});
                //@todo enqueue to call agent after the delivery period +X hours.
            });

            res.send({});
        },

        decline: (req, res) => {
            mixpanel.track('agent-declined', {
                shipment_id: req.params.shipmentID
            });

            shipment.findById(req.params.shipmentID).then(lab360_shipment => {
                lab360_shipment.update({status: 'agent-declined'});

                slack.send({
                    text: `O agente *X* acabou de negar a coleta para o cliente *Y*. Entrar em contato urgente.`,
                    channel: '#beta',
                    username: 'LabConnect (beta)',
                });
            });

            res.send({});

        }
    },
    fetch: (req, res) => {
        vcr.collects.all({
            done: 'N',
            scheduled_date: '2018-09-24'
        }).then(vcr_shipments => {

            let created_shipments = 0;
            mixpanel.track('vcr-fetch-collect');
            const l360_shipments = _.map(vcr_shipments, vcr_shipment => {
                return {
                    type: 'collect',
                    status: 'pending',
                    scheduled_at: vcr_shipment.scheduled_date+' '+vcr_shipment.scheduled_time,
                    vcr_branch: vcr_shipment.branch,
                    vcr_number: vcr_shipment.id
                };
            });

            shipment.bulkCreate(l360_shipments).then(created => {
                res.json({
                    found_shipments: vcr_shipments.length,
                    created_shipments: created_shipments
                });
            });
        });
    },
    process: (req, res) => {
        shipment.all({
            where: {status: 'pending', type: 'collect'}
        }).then(l360_shipments => {
            _.each(l360_shipments, l360_shipment => {
                vcr.collects.find({
                    id: l360_shipment.vcr_number,
                    branch: l360_shipment.vcr_branch
                }).then(vcr_shipment => {
                    twilio.studio.flows(CLIENT_CONFIRM_FLOW).executions.create({
                        to: '+5551984583776',
                        from: TWILIO_FROM,
                        parameters: {
                            shipment_id: req.params.shipmentID,
                            client_name: vcr_shipment.client.name
                        }
                    }).then(execution => {
                        mixpanel.track('client-flow-started', {
                            shipment_id: req.params.shipmentID,
                            flow_sid: execution.flow_sid
                        });

                        res.json(execution);
                    });
                });
            });
        });
    },
    enqueue: (req, res) => {
        res.send('silence is golden');
    }
};