const models = require('../../models');
const vcr = require('../../services/vcr');
const slack = require('../../services/slack');
const {merge} = require('../../helpers');

var tipo = new Array();
tipo['A'] = 'avaria_parcial';
tipo['B'] = 'avaria_total';
tipo['C'] = 'falta_parcial';
tipo['D'] = 'falta_total';
tipo['E'] = 'sobra_parcial';
tipo['F'] = 'sobra_total';
tipo['G'] = 'devolucao_parcial';
tipo['H'] = 'devolucao_total';
tipo['I'] = 'sinistro_parcial';
tipo['J'] = 'sinistro_total';
tipo['K'] = 'erro_calculo';
tipo['L'] = 'atraso_entrega';
tipo['T'] = 'troca_mercadoria';



module.exports = {
    show: (req, res) => {
        models.ticket.findById(req.params.ticketID).then(lab360_ticket => {
            Promise.all([
                vcr.tickets.find({
                    branch: lab360_ticket.vcr_branch,
                    number: lab360_ticket.vcr_number
                }),
                vcr.comments(lab360_ticket.vcr_document)
            ]).then(([vcr_ticket, vcr_comments]) => {
                res.json(merge(lab360_ticket, vcr_ticket, {comments: vcr_comments}));
            });
        });
    }
};