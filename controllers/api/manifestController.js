const vcr = require('../../services/vcr');
const models = require('../../models');
const mixpanel = require('./../../services/mixpanel');

const {merge} = require('../../helpers');
const _ = require('lodash');

module.exports = {
    show: async (req, res) => {
        const l360_manifest = await models.manifest.findById(req.params.manifestID, {
            include: [
                { model: models.scan, as: 'scan'}
            ],
        }).catch(err => {
            res.status(500).send({
                error: 500,
                message: err.message
            });
        });

        if(!l360_manifest){
            res.status(404).send({
                error: 404,
                message: 'manifest not found'
            });
        }

        Promise.all([
            vcr.manifests.find({branch: l360_manifest.vcr_branch, series: l360_manifest.vcr_series, number: l360_manifest.vcr_number}),
            vcr.api.fetch_tags(l360_manifest.vcr_number, l360_manifest.vcr_branch, l360_manifest.vcr_series),
        ]).then(([vcr_manifest, vcr_tags]) => {
            res.json(merge(
                l360_manifest,
                vcr_manifest,
                {tags: _.map(vcr_tags, 'etiq')}
            ));
        });

    },

    scan: (req, res) => {
        const [type, branch, series, number] = req.params.barcode.split('$');

        if(type !== 'M'){
            res.status(401).json({
                error: 401,
                message: 'invalid manifest barcode'
            });
        }

        Promise.all([
            vcr.manifests.find({branch, series, number}),
            vcr.api.fetch_tags(number, branch, series)
        ]).then(([vcr_manifest, vcr_tags]) => {

            if(!vcr_manifest){
                res.status(404).json({
                    error: 404,
                    message: 'Manifest not found.'
                });
            }

            models.manifest.findOrCreate({
                where: {
                    vcr_branch: branch,
                    vcr_series: series,
                    vcr_number: number
                },
                include: [{model: models.scan, as: 'scan'}],
            }).then((lab360_manifest) => {

                const tags = _.uniq(vcr_tags.map(tag => tag.etiq));
                let scanned = [];

                if(lab360_manifest[0].scan !== null){
                    scanned = _.map(lab360_manifest[0].scan.tags, 'qr');
                    delete lab360_manifest[0].scan.tags;
                }

                const pending = _.difference(tags, scanned);

                res.json(merge(lab360_manifest[0], vcr_manifest, {pending, scanned}));
            });
        });
    },

    nfe: (req, res) => {
        vcr.nfe(req.params.manifestID, req.params.nfeID).then(([nfe]) => {
            res.send({
                id: nfe.notnota,
                branch: nfe.notfilial,
                cte: nfe.notconhec,
                weight: nfe.notpeso,
                client: {
                    name: nfe.clinome
                }
            });
        })
    }
};




