const passport = require('passport');
const jwt = require("passport-jwt");

passport.use(new jwt.Strategy({
        jwtFromRequest: jwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey   : process.env.JWT_KEY
    }, function (jwtPayload, done) {
        const models = require("../models");
        return models.user.findById(jwtPayload)
            .then(user => {
                return done(null, user);
            })
            .catch(err => {
                return done(err)
            });
    }
));

module.exports = passport;