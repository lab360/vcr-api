const axios = require('axios');




const client = axios.create({
    baseURL: `https://${process.env.JIRA_HOST}/rest/api/3`,
    auth: {
        username: process.env.JIRA_USERNAME,
        password: process.env.JIRA_PASSWORD
    },
});

client.interceptors.response.use(
    response => response.data,
    error => Promise.reject(error.response.data)
);


module.exports = {
    issue: {
        find: (id) => client.get(`/issue/${id}`),
        create: (params) => client.post('/issue', params)
    },
    type: {
        all: () => client.get('/issuetype'),
        find: (id) => client.get(`/issuetype/${id}`),
        create: (params) => client.post('/issuetype', params)
    },
    project: {
        all: () => client.get('/project'),
        find: (id) => client.get(`/project/${id}`),
        create: (params) => client.post('/project', params)
    },
    priority: {
        all: () => client.get('/priority'),
        find: (id) => client.get(`/priority/${id}`),
        create: (params) => client.post('/priority', params)
    },
    resolution: {
        all: () => client.get('/resolution'),
        find: (id) => client.get(`/resolution/${id}`),
        create: (params) => client.post('/resolution', params)
    },
    status: {
        all: () => client.get('/status'),
        find: (id) => client.get(`/status/${id}`),
        create: (params) => client.post('/status', params)
    },
    ...client
};