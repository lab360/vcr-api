const passport = require('passport');
const local = require('passport-local');
const _ = require('lodash');

const models = require('../models');
const vcr = require('../services/vcr');
const {merge} = require('../helpers');

passport.serializeUser((user, done) => done(null, user));
passport.deserializeUser((user, done) => done(null, user));

passport.use('login', new local.Strategy({ passReqToCallback : true }, (req, username, password, done) => {
    vcr.users.find({
        username: _.toUpper(username),
        password: password
    }).then(vcr_user => {

        if(!vcr_user) return done(null, false);

        models.user.findOrCreate({ where: {vcr_username: _.toUpper(username)} }).then(lab360_user => {
            return done(null, merge(lab360_user[0], vcr_user));
        });
    });
}));

module.exports = passport;