const Sequelize = require('sequelize');

const options = {
    host: process.env.DB_HOST,
    dialect: 'postgres',
    operatorsAliases: false,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },

    define: {
        underscored: true,
        raw: true
    }
};

if(process.env.DATABASE_URL){
    module.exports = new Sequelize(process.env.DATABASE_URL, options);
} else {
    module.exports = new Sequelize(process.env.DB_DATABASE, process.env.DB_USER, process.env.DB_PASSWORD, options);
}

