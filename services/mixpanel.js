/**
 * @type {mixpanel.Mixpanel}
 */
module.exports = require('mixpanel').init(process.env.MIXPANEL_TOKEN);