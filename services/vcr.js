const Sequelize = require('sequelize');
const _ = require('lodash');

const db = new Sequelize(process.env.VCR_DATABASE, process.env.VCR_USER, process.env.VCR_PASSWORD, {
    host: process.env.VCR_HOST,
    dialect: 'mysql',
    operatorsAliases: false,

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },

    define: {
        timestamps: false,
        engine: 'MYISAM',
        freezeTableName: true
    }
});

const constants =  {
    TICKET_CREATED: 'ticket-created',
    COMMENT_CREATED: 'comment-created',
    LOG_TAGS: 'log-tags'
};

const User = db.define('tabusuarios', {
    username: {
        type: Sequelize.STRING,
        primaryKey: true,
        field: 'usercod'
    },
    password: {
        type: Sequelize.STRING,
        field: 'usersenha'
    }
});

const Ticket = db.define('boletim', {
    type_id: {
        type: Sequelize.STRING,
        field: 'boltipo'
    },
    jira_id: {
        type: Sequelize.INTEGER,
        field: 'boljira'
    },
    branch: {
       type: Sequelize.STRING,
       primaryKey:true,
       field: 'bolfemi'
    },
    branch_assigned: {
        type: Sequelize.STRING,
        field: 'bolfilr'
    },
    branch_manifest: {
        type: Sequelize.STRING,
        field: 'bolfilman'
    },
    branch_document: {
        type: Sequelize.STRING,
        field: 'bolfilc'
    },
    origin: {
        type: Sequelize.STRING,
        field: 'bolremet'
    },
    destination: {
        type: Sequelize.STRING,
        field: 'boldestin'
    },
    number: {
       type: Sequelize.INTEGER,
       primaryKey: true,
       field: 'bolnumero'
    },
    series: {
       type: Sequelize.INTEGER,
       primaryKey:true,
       field: 'bolserie'
    },
    title: {
        type: Sequelize.STRING,
        field: 'boldecl'
    },
    document: {
       type: Sequelize.INTEGER,
       field: 'bolnota'
    },
    closed_date: {
       type: Sequelize.STRING,
       field: 'boldataenc',
    },
    closed_time: {
       type: Sequelize.STRING,
       field:'bolhoraenc',
    },
    opened_date: {
        type: Sequelize.STRING,
        field: 'boldemi'
    },
    opened_time: {
        type: Sequelize.STRING,
        field: 'bolhora'
    },
    closed_at: {
        type: Sequelize.VIRTUAL,
        get(){
            if(this.getDataValue('closed_date')){
                return this.getDataValue('closed_date') + 'T' + this.getDataValue('closed_time');
            }

            return null;
        }
    },
    solved: {
       type: Sequelize.STRING,
       field: 'bolsol',
       get() {
           return this.getDataValue('solved') == 'S' ? true : false;
       },
       set(value) {
           this.setDataValue('solved', value === true ? 'S' : 'N');
       }
    }
}, {
    hidden: ['closed_time','closed_date', 'type_id'],
    hooks: {
        beforeFind: (options) => {
            if(options.where.solved !== undefined){
                options.where.solved = options.where.solved === true ? 'S' : 'N';
            }
        },
        beforeCount: (options) => {
            if(options.where.solved !== undefined){
                options.where.solved = options.where.solved === true ? 'S' : 'N';
            }
        }
    }
});

const TicketType = db.define('boletimtipo', {
    type_id: {
        type: Sequelize.STRING,
        primaryKey:true,
        field: 'tipcod'
    },
    jira_id: {
        type: Sequelize.INTEGER,
        field: 'tipjira'
    },
    title: {
        type: Sequelize.STRING,
        field: 'tipdescr'
    }
});

Ticket.belongsTo(TicketType, {as: 'type', foreignKey: 'type_id'});

const TicketComment = db.define('boletimhis', {
    branch: {
        type: Sequelize.STRING,
        primaryKey: true,
        field: 'hisfemi'
    },
    number: {
        type: Sequelize.INTEGER,
        primaryKey:true,
        field: 'hisnumero'
    },
    comment: {
        type: Sequelize.TEXT,
        field: 'histexto'
    }
});
Ticket.hasMany(TicketComment, {as: 'comments', foreignKey: 'number'});

const Manifest = db.define('tabmanifestos', {
    branch: {
        type: Sequelize.STRING,
        primaryKey:true,
        field: 'manfilial'
    },
    number: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        field: 'manmanife'
    },
    series: {
        type: Sequelize.INTEGER,
        primaryKey:true,
        field: 'manserie'
    },
    driver_id: {
        type: Sequelize.STRING,
        field: 'mancpfmot'
    },
    plate: {
        type: Sequelize.STRING,
        field: 'manplaca'
    },
    origin: {
        type: Sequelize.STRING,
        field: 'manfilori'
    },
    destination: {
        type: Sequelize.STRING,
        field: 'manfdes'
    }
});

const Driver = db.define('tabmotoristas', {
   cpf: {
       type: Sequelize.INTEGER,
       primaryKey: true,
       field: 'motcgc',
   },
   type: {
       type: Sequelize.STRING,
       field: 'motid'
   },
   name: {
       type: Sequelize.STRING,
       field: 'motnome'
   },
   phone: {
       type: Sequelize.STRING,
       field: 'motcelular'
   },
   license_category: {
       type: Sequelize.STRING,
       field: 'motcat'
   },
   license_number: {
       type: Sequelize.STRING,
       field: 'motcnh'
   }
});

Manifest.belongsTo(Driver, {as: 'driver', foreignKey: 'driver_id'});

const Vehicle = db.define('tabveiculos', {
    plate: {
        type: Sequelize.STRING,
        primaryKey: true,
        field: 'veiplac'
    },
    brand: {
        type: Sequelize.STRING,
        field: 'veimarc'
    },
    year: {
        type: Sequelize.INTEGER,
        field: 'veiano'
    },
    model: {
        type: Sequelize.STRING,
        field: 'veimodelo'
    },
    city: {
        type: Sequelize.STRING,
        field: 'veimun'
    },
    state: {
        type: Sequelize.STRING,
        field: 'veiest'
    },
    axis: {
        type: Sequelize.INTEGER,
        field: 'veinumeixo'
    },
    capacity: {
        type: Sequelize.STRING,
        field: 'veicapac'
    },
    renavam: {
        type: Sequelize.STRING,
        field: 'veirenavam'
    },
    antt: {
        type: Sequelize.STRING,
        field: 'veiantt'
    }
});
Manifest.belongsTo(Vehicle, {as: 'vehicle', foreignKey: 'plate'});

const Client = db.define('tabclientes', {
    id: {
        type: Sequelize.STRING,
        primaryKey: true,
        field: 'clicgc'
    },
    name: {
        type: Sequelize.STRING,
        field: 'clinome'
    }
});

const NFE = db.define('tabnotas', {
    id: {
        type: Sequelize.STRING,
        primaryKey: true,
        field: 'notnota'
    },
    client_id: {
        type: Sequelize.STRING,
        field: 'notcgcr'
    }

});
NFE.belongsTo(Client, {as: 'client', foreignKey: 'client_id'});

const CollectClient = db.define('tabclicol', {
    id: {
        type: Sequelize.STRING,
        primaryKey: true,
        field: 'clccgc'
    },
    name: {
        type: Sequelize.STRING,
        field: 'clcnome'
    },
    address: {
        type: Sequelize.STRING,
        field: 'clcend'
    },
    district: {
        type: Sequelize.STRING,
        field: 'clcbairro'
    },
    city: {
        type: Sequelize.STRING,
        field: 'clccid'
    },
    zip: {
        type: Sequelize.STRING,
        field: 'clccep'
    },
    phone: {
        type: Sequelize.STRING,
        field: 'clcfone'
    },
    contact: {
        type: Sequelize.STRING,
        field: 'clcsolic'
    }
});

const Collect = db.define('tabcoleta', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        field: 'colnumero'
    },
    client_id: {
        type: Sequelize.INTEGER,
        field: 'colfoner'
    },
    branch: {
        type: Sequelize.STRING,
        primaryKey: true,
        field: 'colfilial'
    },
    scheduled_date: {
        type: Sequelize.STRING,
        field: 'coldata'
    },
    scheduled_time: {
        type: Sequelize.STRING,
        field: 'colhora'
    },
    done: {
        type: Sequelize.STRING,
        field: 'colrealizado'
    },
    chemical: {
        type: Sequelize.STRING,
        field: 'colquimico'
    },
    risk: {
        type: Sequelize.STRING,
        field: 'colrisco'
    }
});
Collect.belongsTo(CollectClient, {as: 'client', foreignKey: 'client_id'});

const axios = require('axios');

module.exports = {
    users: {
        all: (where = {}, options) => {
            return User.all({where: where, ...options });
        },
        find: (where = {}) => {
            return User.findOne({where: where});
        }
    },
    tickets: {
        all: (where = {}, options) => {
            /**
             * @returns Ticket
             */
            return Ticket.all({
                where: where,
                include: [{model: TicketType, as: 'type'}],
                ...options
            });
        },
        find: (where = {}, options) => {
            return Ticket.findOne({
                where: where,
                include: [{model: TicketType, as: 'type'},{model: TicketComment, as: 'comments'}],
                ...options
            });
        }
    },
    manifests: {
        all: (where = {}, options) => {
            return Manifest.all({
                where: where,
                include: [{model: Driver, as: 'driver'}, {model: Vehicle, as: 'vehicle'}],
                ...options
            });
        },
        find: (where = {}) => {
            return Manifest.findOne({
                where: where,
                include: [{model: Driver, as: 'driver'}, {model: Vehicle, as: 'vehicle'}],
            });
        }
    },
    collects: {
        all: (where = {}, options) => {
            return Collect.all({
                where: where,
                include: [{model: CollectClient, as: 'client'}],
                ...options
            });
        },
        find: (where = {}) => {
            return Collect.findOne({
                where: where,
                include: [{model: CollectClient, as: 'client'}],
            });
        }
    },
    drivers: {
        all: (where = {}, options) => {
            return Driver.all({where: where});
        },
        find: (where = {}) => {
            return Driver.findOne({where: where});
        }
    },
    vehicles: {
        all: (where = {}, options) => {
            return Vehicle.all({ raw:true, where: where});
        },
        find: (where = {}) => {
            return Vehicle.findOne({raw: true, where: where});
        }
    },
    clients: {
        all: (where = {}, options) => {
            return Client.all({ raw:true, where: where});
        },
        find: (where = {}) => {
            return Client.findOne({raw: true, where: where});
        }
    },

    scanned_tags: (branch, series, number) => {
        return db.query('SELECT tabetiqnf.etibarra AS barcode FROM tabetilei INNER JOIN tabetiqnf ON tabetilei.leivol = tabetiqnf.etivol AND tabetilei.leilanc = tabetiqnf.etilanc WHERE leidoc LIKE :doc', {
            replacements: {doc: `%-%-${branch}-${series}-${number}`},
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        });
    },

    comments: (document) => {
        return db.query('SELECT histexto AS text, CONCAT(CAST(hisdata AS CHAR), "T", CAST(hishora AS CHAR)) AS created_at FROM boletimhis WHERE hisnumero = :document ORDER BY hisdata, hishora', {
          replacements: {document},
          type: Sequelize.QueryTypes.SELECT
        });
    },

    nfe: (manifest_id, nfe_id) => {
        return new Promise((resolve, reject) => {
            db.query('SELECT tabnotas.*, tabclientes.clinome FROM tabnotas INNER JOIN tabclientes ON tabnotas.notcgcr=tabclientes.`clicgc` WHERE notconhec  IN  (SELECT incon FROM tabitenmanife WHERE imanife = :manifest_id) AND notnota= :nfe_id ',{
                replacements: {manifest_id, nfe_id},
                model: NFE,
                type: Sequelize.QueryTypes.SELECT,
                raw: true
            }).then(([nfe]) => resolve({
                number: nfe.notnota,
                branch: nfe.notfilial,
                cte: nfe.notconhec,
                weight: nfe.notpeso,
                client: {
                    name: nfe.clinome
                }
            })).catch(err => reject(err));
        });
    },

    models: { Ticket, Vehicle, Driver, Manifest, User, NFE, Collect },
    api: {
        log_scan: (data) => {
            return new Promise((resolve, reject) => {
                axios.post('http://www.rodoviasullog.com.br/recleituratot.php', data).then(response => {
                    if(response.data === ''){
                        resolve();
                    } else {
                        reject(new Error(`VCR: ${response.data}`));
                    }
                }).catch(err => reject(new Error(`VCR error 500`)));
            });
        },
        fetch_tags: (number, branch, series = 1) => {
            return new Promise((resolve, reject) => {
                axios.get(`http://www.rodoviasullog.com.br/retetiq.asp?manife=${number}&filial=${branch}&serie=${series}&tipoman=M`).then(response => {
                    if(_.has(response.data, 'erro')){
                        reject(new Error(`VCR: ${response.data.erro}`));
                    }

                    resolve(response.data);
                }).catch(err => reject(new Error(`VCR error 500`)));
            });
        }
    },
    db: db,
    constants: constants
};
