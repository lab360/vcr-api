const express = require('express');

const {log} = require('../helpers');
const {constants} = require('../services/vcr');

/** WEB Routes Definition **/
const web = express.Router();

const mainController = require('../controllers/mainController');
web.get('/', mainController.index);
web.get('/auth/:id', mainController.token);
web.post('/auth', mainController.auth);


const scanController = require('../controllers/api/scanController');
web.get('/log-everything', scanController.logEverything);

/** Routes below this point will handle mostly webhooks from third party services **/
const vcrController = require('../controllers/vcrController');
web.get('/vcr', vcrController.status);
web.post('/vcr/tickets',
    (req, res, next) => log(constants.TICKET_CREATED, 'webhook', req.body).then(() => next()),
    vcrController.tickets
);
web.post('/vcr/tickets/comments',
    (req, res, next) => log(constants.COMMENT_CREATED, 'webhook', req.body).then(() => next()),
    vcrController.comments
);

const jiraController = require('../controllers/jiraController');
web.post('/jira/ticket',
    (req, res, next) => log('jira-ticket', 'webhook', req.body).then(() => next()),
    jiraController.ticket
);

web.post('/jira/comment',
    (req, res, next) =>  log('jira-comment', 'webhook', req.body).then(() => next()),
    jiraController.comment
);

module.exports = web;
