const api = require('express').Router();

//const jwt = require('../services/jwt');
//api.use(jwt.authenticate('jwt', {session: false}));


/** Tickets **/
const ticketController = require('../controllers/api/ticketController');

/**
 * @swagger
 * /tickets/{ticketID}:
 *   get:
 *     tags:
 *       - Tickets
 *     description: Returns a single ticket
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: ticketID
 *         description: Ticket's id
 *         in: path
 *         required: true
 *         type: string
 *         format: uuid
 *     responses:
 *       200:
 *         description: A single ticket
 *         schema:
 *           $ref: '#/definitions/Ticket'
 *
 */
api.get('/tickets/:ticketID', ticketController.show);

/** Users **/
const userController = require('../controllers/api/userController');
api.post('/users/', userController.create);

/**
 * @swagger
 * /users/{userID}:
 *   get:
 *     tags:
 *       - Users
 *     description: Returns a single user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: userID
 *         description: Manifest's id
 *         in: path
 *         required: true
 *         type: string
 *         format: uuid
 *     responses:
 *       200:
 *         description: A single user
 *         schema:
 *           $ref: '#/definitions/User'
 *
 */
api.get('/users/:userID', userController.show);

/** Manifests **/
const manifestController = require('../controllers/api/manifestController');
api.get('/manifests/scan/:barcode', manifestController.scan);

/**
 * @swagger
 * /manifests/{manifestID}:
 *   get:
 *     tags:
 *       - Manifests
 *     description: Returns a single manifest
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: manifestID
 *         description: Manifest's id
 *         in: path
 *         required: true
 *         type: string
 *         format: uuid
 *     responses:
 *       200:
 *         description: A single manifest
 *         schema:
 *           $ref: '#/definitions/Manifest'
 *
 */
api.get('/manifests/:manifestID', manifestController.show);
api.get('/manifests/:manifestID/nfe/:nfeID', manifestController.nfe);

/** Scans **/
const scanController = require('../controllers/api/scanController');

/**
 * @swagger
 * /scans:
 *   get:
 *     tags:
 *       - Scans
 *     description: Returns all scans
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of scans
 *         schema:
 *           $ref: '#/definitions/Scan'
 */
api.get('/scans/', scanController.index);

/**
 * @swagger
 * /scans:
 *   post:
 *     tags:
 *       - Scans
 *     description: Creates a new scan
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: scan
 *         description: Scan object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/Scan'
 *     responses:
 *       200:
 *         description: Successfully created
 */
api.post('/scans/', scanController.create);

/**
 * @swagger
 * /scans/{scanID}:
 *   get:
 *     tags:
 *       - Scans
 *     description: Returns a single scan process
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: scanID
 *         description: Scan's id
 *         in: path
 *         required: true
 *         type: string
 *         format: uuid
 *       - name: scan
 *         description: Scan object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/Scan'
 *     responses:
 *       200:
 *         description: A single scan
 *         schema:
 *           $ref: '#/definitions/Scan'
 *
 */
api.get('/scans/:scanID', scanController.show);

/**
 * @swagger
 * /scans/{scanID}:
 *   put:
 *     tags:
 *       - Scans
 *     description: Updates a single scan process
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: scanID
 *         description: Scan's id
 *         in: path
 *         required: true
 *         type: string
 *         format: uuid
 *     responses:
 *       200:
 *         description: A single scan
 *         schema:
 *           $ref: '#/definitions/Scan'
 *
 */
api.put('/scans/:scanID', scanController.update);

/**
 * @swagger
 * /scans/{scanID}/log:
 *   post:
 *     tags:
 *       - Scans
 *     description: Log tags to a scan process
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: scanID
 *         description: Scan's id
 *         in: path
 *         required: true
 *       - name: scanned
 *         description: array containing all scanned tags
 *         in: body
 *         required: true
 *       - name: invalid
 *         description: array containing all invalid tags
 *         in: body
 *         required: true
 *     responses:
 *       200:
 *         description: Successfully created
 */
api.post('/scans/:scanID/log', scanController.log);

/** Events **/
const eventController = require('../controllers/api/eventController');

/**
 * @swagger
 * /events:
 *   get:
 *     tags:
 *       - Events
 *     description: Returns all events
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of events
 *         schema:
 *           $ref: '#/definitions/Event'
 */
api.get('/events/', eventController.index);

/**
 * @swagger
 * /events:
 *   post:
 *     tags:
 *       - Events
 *     description: Creates a new event
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: event
 *         description: Event object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/Event'
 *     responses:
 *       200:
 *         description: Successfully created
 */
api.post('/events/', eventController.create);
api.get('/events/:eventID', eventController.show);

/** Drivers **/
const driverController = require('../controllers/api/driverController');

/**
 * @swagger
 * /drivers/search/{cpf}:
 *   get:
 *     tags:
 *       - Drivers
 *     description: Returns a single driver by its CPF number, if exists.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: cpf
 *         description: Driver's CPF
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: A single driver
 *         schema:
 *           $ref: '#/definitions/Driver'
 *
 */
api.get('/drivers/search/:cpf', driverController.search);

/**
 * @swagger
 * /drivers/{driverID}:
 *   get:
 *     tags:
 *       - Drivers
 *     description: Returns a single driver
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: driverID
 *         description: Driver's id
 *         in: path
 *         required: true
 *         type: string
 *         format: uuid
 *     responses:
 *       200:
 *         description: A single driver
 *         schema:
 *           $ref: '#/definitions/Driver'
 *
 */
api.get('/drivers/:driverID', driverController.show);

/** Drivers **/
const vehicleController = require('../controllers/api/vehicleController');

/**
 * @swagger
 * /vehicles/search/{plate}:
 *   get:
 *     tags:
 *       - Vehicles
 *     description: Returns a single vehicle by its plate number, if exists.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: plate
 *         description: Vehicle's Plate
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: A single vehicle
 *         schema:
 *           $ref: '#/definitions/Vehicle'
 *
 */
api.get('/vehicles/search/:cpf', vehicleController.search);

/**
 * @swagger
 * /vehicles/{vehicleID}:
 *   get:
 *     tags:
 *       - Vehicles
 *     description: Returns a single vehicle
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: vehicleID
 *         description: Vehicle's id
 *         in: path
 *         required: true
 *         type: string
 *         format: uuid
 *     responses:
 *       200:
 *         description: A single vehicle
 *         schema:
 *           $ref: '#/definitions/Vehicle'
 *
 */
api.get('/vehicles/:vehicleID', vehicleController.show);

/** Shipments **/
const shipmentContoller = require('../controllers/api/shipmentController');
api.get('/shipments/fetch', shipmentContoller.fetch);
api.get('/shipments/process', shipmentContoller.process);
api.post('/shipments/:shipmentID/queue', shipmentContoller.enqueue);
api.post('/shipments/:shipmentID/client-confirm', shipmentContoller.client.confirm);
api.post('/shipments/:shipmentID/client-decline', shipmentContoller.client.decline);
api.post('/shipments/:shipmentID/agent-confirm', shipmentContoller.agent.confirm);
api.post('/shipments/:shipmentID/agent-decline', shipmentContoller.agent.decline);

/** Docks **/
const dockController = require('../controllers/api/dockController');

/**
 * @swagger
 * /docks:
 *   get:
 *     tags:
 *       - Docks
 *     description: Returns all truck docks
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of truck docks
 *         schema:
 *           $ref: '#/definitions/Dock'
 */
api.get('/docks', dockController.index);

/**
 * @swagger
 * /docks/{dockID}:
 *   get:
 *     tags:
 *       - Docks
 *     description: Returns a specified truck docks
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: dockID
 *         description: Dock's id
 *         in: path
 *     responses:
 *       200:
 *         description: The specified Dock object
 *         schema:
 *           $ref: '#/definitions/Dock'
 */
api.get('/docks/:dockID', dockController.show);

const nfeController = require('../controllers/api/nfeController');
api.get('/nfes/:nfeID', nfeController.show);

module.exports = api;