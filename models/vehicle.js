module.exports = (sequelize, dataTypes) => {
    const vehicle = sequelize.define('vehicle', {
        id: {
            type: dataTypes.UUID,
            defaultValue: dataTypes.UUIDv4,
            primaryKey: true
        },
        plate: {
            type: dataTypes.STRING,
        },
        type: {
            type: dataTypes.STRING
        }
    });

    return vehicle;
};