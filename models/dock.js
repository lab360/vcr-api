/**
 * @swagger
 * definitions:
 *   Dock:
 *     properties:
 *       id:
 *         type: string
 *         format: uuid
 *       vehicle_id:
 *          type: string
 *          format: uuid
 *       driver_id:
 *          type: string
 *          format: uuid
 *       name:
 *          type: string
 *       created_at:
 *         type: string
 *       updated_at:
 *         type: string
 *
 */

module.exports = (sequelize, dataTypes) => {
	const dock = sequelize.define('dock', {
		id: {
			type: dataTypes.UUID,
			defaultValue: dataTypes.UUIDv4,
			primaryKey: true
		},
		vehicle_id: {
			type: dataTypes.UUID
		},
		driver_id: {
			type: dataTypes.UUID
		},
		name: {
			type: dataTypes.ENUM('arrival', 'departure')
		}
	});
	
	dock.associate = function (models) {
		models.dock.belongsTo(models.vehicle, {
			as: 'vehicle',
			foreignKey: 'vehicle_id'
		});
	};
	
	return dock;
};