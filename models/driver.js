module.exports = (sequelize, dataTypes) => {
    const driver = sequelize.define('driver', {
        id: {
            type: dataTypes.UUID,
            defaultValue: dataTypes.UUIDv4,
            primaryKey: true
        },
        vcr_cpf: {
            type: dataTypes.STRING,
        },
        nickname: {
            type: dataTypes.STRING
        }
    }, {
        hidden: 'vcr_cpf'
    });

    return driver;
};