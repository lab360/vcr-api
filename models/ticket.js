module.exports = (sequelize, dataTypes) => {
    const ticket = sequelize.define('ticket', {
        id: {
            type: dataTypes.UUID,
            defaultValue: dataTypes.UUIDv4,
            primaryKey: true
        },
        vcr_branch: {
            type: dataTypes.STRING,
        },
        vcr_number: {
            type: dataTypes.INTEGER,
        },
        vcr_document: {
            type: dataTypes.INTEGER
        }
    }, {
        hidden: ['vcr_branch', 'vcr_number', 'vcr_document']
    });

    return ticket;
};