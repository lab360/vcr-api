/**
 * @swagger
 * definitions:
 *   Event:
 *     properties:
 *       id:
 *         type: string
 *         format: uuid
 *       vehicle_id:
 *         type: string
 *         format: uuid
 *       dock_id:
 *         type: string
 *         format: uuid
 *       user_id:
 *         type: string
 *         format: uuid
 *       manifest_id:
 *         type: string
 *         format: uuid
 *       driver_id:
 *         type: string
 *         format: uuid
 *       type:
 *         type: string
 *       note:
 *         type: string
 *       created_at:
 *          type: string
 *          format: date
 *       updated_at:
 *          type: string
 *          format: date
 */


module.exports = (sequelize, dataTypes) => {
    const event = sequelize.define('event', {
        id: {
            type: dataTypes.UUID,
            defaultValue: dataTypes.UUIDv4,
            primaryKey: true
        },
        vehicle_id: {
            type: dataTypes.UUID
        },
        dock_id: {
            type: dataTypes.UUID
        },
        user_id: {
            type: dataTypes.UUID
        },
        manifest_id: {
            type: dataTypes.UUID,
        },
        type: {
            type: dataTypes.ENUM('arrival', 'departure')
        },
        note: {
            type: dataTypes.STRING,
        },
        driver: {
            type: dataTypes.STRING
        }
    });

    return event;
};
