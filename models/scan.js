/**
 * @swagger
 * definitions:
 *   Scan:
 *     properties:
 *       id:
 *         type: string
 *         format: uuid
 *       user_id:
 *         type: string
 *         format: uuid
 *       manifest_id:
 *         type: string
 *         format: uuid
 *       status:
 *         type: string
 *       type:
 *         type: string
 *       branch:
 *         type: string
 *       created_at:
 *          type: string
 *          format: date
 *       updated_at:
 *          type: string
 *          format: date
 */



module.exports = (sequelize, dataTypes) => {
    const scan = sequelize.define('scan', {
        id: {
            type: dataTypes.UUID,
            defaultValue: dataTypes.UUIDv4,
            primaryKey: true
        },
        user_id: {
            type: dataTypes.UUID,
        },
        manifest_id: {
            type: dataTypes.UUID,
        },
        status: {
            type: dataTypes.ENUM('active', 'paused', 'pending', 'finished', 'canceled')
        },
        type: {
            type: dataTypes.ENUM('load', 'unload')
        },
        branch: {
            type: dataTypes.STRING
        },
        tags: {
            type: dataTypes.JSON
        }
    });

    scan.associate = function(models) {
        models.scan.belongsTo(models.user, {as: 'user', foreignKey: 'user_id'});
        models.scan.belongsTo(models.manifest,  {as: 'manifest', foreignKey: 'manifest_id'});
    };

    return scan;
};

