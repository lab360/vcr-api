const fs = require('fs');
const path = require('path');
const sequelize = require('../services/db');

let db = {};

fs.readdirSync(__dirname).filter(file => {
    return (file.indexOf('.') !== 0) && (file !== path.basename(__filename)) && (file.slice(-3) === '.js');
}).forEach(file => {
    const model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
});

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;

/** @namespace db.dock */
/** @namespace db.vehicle */
/** @namespace db.event */
/** @namespace db.scan */
module.exports = db;