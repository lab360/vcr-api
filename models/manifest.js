module.exports = (sequelize, dataTypes) => {
    const manifest = sequelize.define('manifest', {
        id: {
            type: dataTypes.UUID,
            defaultValue: dataTypes.UUIDv4,
            primaryKey: true
        },
        vcr_branch: {
            type: dataTypes.STRING,
        },
        vcr_number: {
            type: dataTypes.INTEGER,
        },
        vcr_series: {
            type: dataTypes.INTEGER,
        }
    }, {
        hidden: ['vcr_branch', 'vcr_number', 'vcr_series']
    });

    manifest.associate = function(models){
        models.manifest.hasOne(models.scan, {as: 'scan', foreignKey: 'manifest_id'});
    };

    return manifest;
};