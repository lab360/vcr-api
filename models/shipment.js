module.exports = (sequelize, dataTypes) => {
    return sequelize.define('shipment', {
        id: {
            type: dataTypes.UUID,
            defaultValue: dataTypes.UUIDv4,
            primaryKey: true
        },
        type: {
            type: dataTypes.ENUM('delivery', 'collect')
        },
        status: {
            type: dataTypes.STRING
        },
        scheduled_at: {
            type: dataTypes.STRING
        },
        vcr_branch: {
            type: dataTypes.STRING
        },
        vcr_number: {
            type: dataTypes.INTEGER
        }
    });
};

