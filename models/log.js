module.exports = (sequelize, dataTypes) => {
    const log = sequelize.define('log', {
        id: {
            type: dataTypes.UUID,
            defaultValue: dataTypes.UUIDv4,
            primaryKey: true
        },
        type: {
            type: dataTypes.ENUM('webhook', 'api', 'other')
        },
        key: {
            type: dataTypes.STRING
        },
        data: {
            type: dataTypes.JSON
        }
    });

    return log;
};