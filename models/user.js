module.exports = (sequelize, dataTypes) => {
    const user = sequelize.define('user', {
        id: {
            type: dataTypes.UUID,
            defaultValue: dataTypes.UUIDv4,
            primaryKey: true
        },
        team: {
            type: dataTypes.STRING
        },
        name: {
            type: dataTypes.STRING
        },
        role: {
            type: dataTypes.ENUM('user', 'admin')
        },
        vcr_username: {
            type: dataTypes.STRING
        },
    }, {
        hidden: 'vcr_username'
    });

    return user;
};
