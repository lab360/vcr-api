const _ = require('lodash');
const slack = require('./services/slack');
const {log} = require('./models');

module.exports = {
    merge: (...objects) => {
        return _.merge(...objects.map(obj => {
            if(_.has(obj, 'dataValues')) {
                return _.omit(obj.dataValues, obj._modelOptions.hidden || []);
            }
            return obj;
        }));
    },
    restrict:  (req, res, next) => {
        if (req.isAuthenticated()) {
            return next()
        }

        req.session.returnTo = req.path;
        res.redirect('/login');
    },
    slack: (message, channel = '#beta') => {
        slack.send({
            text: message,
            channel: channel,
            username: 'Rodovia Sul (Beta)'
        });
    },
    log: (key, type = 'webhook', data) => {
        return log.create({ key, type, data });
    }
};