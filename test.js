require('dotenv').config();

const axios = require("axios");
const chai = require("chai");
const expect = chai.expect;

//const vcr = require('./services/vcr');

// describe('Test VCR fetch_tags endpoint', function(){
//
//     it("should return 200", function(done){
//         axios.get('http://www.rodoviasullog.com.br/retetiq.asp?manife=2487&filial=CUR&serie=1&tipoman=M').then(response => {
//             expect(response.status).to.equal(200);
//             done();
//         });
//     });
//
//     it("should return a valid array", function(done){
//         axios.get('http://www.rodoviasullog.com.br/retetiq.asp?manife=2487&filial=CUR&serie=1&tipoman=M').then(response => {
//             expect(response.data).to.be.an('array');
//             done();
//         });
//     });
// });

// describe('Test VCR log_scan endpoint', async function(){
//
//     const volume = Math.floor(Math.random() * 10000);
//     const etibarra = `Nx17202x33433x${volume}`;
//     const etilanc = 33433;
//     const query = await vcr.db.query(`UPDATE tabetiqnf SET etibarra = '${etibarra}' WHERE etilanc = ${etilanc}`);
//
//     after(() => vcr.db.close());
//
//     it('should return 200 with no data and save it to DB', function(done){
//
//        const randomID = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
//        axios.post('http://www.rodoviasullog.com.br/recleituratot.php', [{
//            filial: "CUR",
//            filiallocal: "POA",
//            serie: 1,
//            manifesto: 2487,
//            tipoman: "M",
//            tipoope: "D",
//            barra: etibarra,
//            usuario: randomID,
//            senha: randomID
//        }]).then(response => {
//            vcr.db.query(`SELECT * FROM tabetilei WHERE leiusu = '${randomID}' LIMIT 1`, {raw: true, type: vcr.db.QueryTypes.SELECT}).then(results => {
//                expect(results).to.be.an('array');
//
//                chai.assert.equal(results.length, 1);
//
//                expect(results[0]).to.include({
//                    leivol: volume,
//                    leitipo: 'D',
//                    leifil: 'CUR',
//                    leiusu: randomID
//                }, 'it does not have the desired object into database');
//
//                done();
//            }).catch(err => done(err));
//        }).catch(err => done(err));
//     });
// });